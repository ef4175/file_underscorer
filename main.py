import shutil
import os


def get_files(path):
    all_files = [file for file in os.listdir(path) if
                 os.path.isfile(os.path.join(path, file))]
    return all_files

def rename_file(file):
    underscored = ""
    for char in file:
        if char == " ":
            underscored += "_"
        else:
            underscored += char
    return underscored
    
path = "path goes here"

all_files = get_files(path)

for file in all_files:
    underscored_file_name = rename_file(file)
    shutil.move(path+file, path+underscored_file_name)

